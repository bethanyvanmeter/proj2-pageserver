from flask import Flask, render_template, make_response

app = Flask(__name__)

@app.route("/")
def index():
    return render_template('trivia.html')

@app.route("/<path:url>")
def routing(url):
	if (".." in url) or ("//" in url) or ("~" in url):
		return make_response(render_template("403.html"), 403)
	try: 
		with open(("templates/" + url), 'r', encoding='utf-8') as source:
			return make_response(render_template(url), 200)
	# handle if there is no file/forbidden access
	except OSError as error:
		return error_404()

@app.errorhandler(403)
def error_403():
	#return a template with error code
	return make_response(render_template("403.html"), 403)

@app.errorhandler(404)
def error_404():
	#return a template with error code
	return make_response(render_template("404.html"), 404)

if __name__ == "__main__":
	# TODO: delete the port
    app.run(debug=True,host='0.0.0.0', port='6021')
