# README #

## Author: Bethany Van Meter, bvanmet2@uoregon.edu ##

* Go to the web folder in the repository.

* Build the image using:
`docker build -t UOCIS-flask-demo .`

* Run the container using:
`docker run -d -p 5000:5000 UOCIS-flask-demo`

* Launch http://127.0.0.1:5000 using web broweser and check the output of trivia.html as the "index" page!


* The goal of this project was to implement the same "file checking" logic that we implemented in project 1 using flask. 

* Like project 1, if a file ("name.html") exists, this project transmits "200/OK" header followed by that file html. If the file doesn't exist, it transmits an error code in the header along with the appropriate page html in the body: 
    * "404.html" will display "File not found!"
    * "403.html" will display "File is forbidden!"
    